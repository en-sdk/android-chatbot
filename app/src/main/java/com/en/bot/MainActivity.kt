package com.en.bot

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.en.botsdk.BotConfig
import com.en.botsdk.CustomAlignment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        BotConfig.getInstance().init("7b2f8fe62c8d4176", "botName", true, "default",
                this, "botUserId", 100, false, CustomAlignment.DEFAULT,
                null, null, R.drawable.icon_send)
        BotConfig.getInstance().launchBot()
//        supportFragmentManager.beginTransaction().add(
//            R.id.fl_root, BotConfig.getInstance().getBotFragment(), null).commit()
    }
}