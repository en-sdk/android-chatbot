package com.en.botsdk

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import androidx.annotation.Keep
import androidx.fragment.app.Fragment

/**
 * Created by NiteshGoyal on 19/07/20.
 */
class BotConfig private constructor() {

    @Keep
    companion object {
        private lateinit var botConfig: BotConfig
        fun getInstance(): BotConfig {
            if (!::botConfig.isInitialized) {
                botConfig = BotConfig()
            }
            return botConfig
        }
    }

    fun init(
        botKey: String?,
        botName: String?,
        welcomeMsg: Boolean,
        brandingKey: String?,
        activity: Activity?,
        userId: String?,
        chatLimitHistory: Int,
        isCallBackEnabled: Boolean,
        isRTL: CustomAlignment?,
        headerTitleFont: Typeface?,
        headerDescriptionFont: Typeface?,
        sendButtonDrawable: Int
    ) {
        /*ChatBotConfig.getInstance().init(
            botKey,
            botName,
            welcomeMsg,
            brandingKey,
            activity,
            userId,
            chatLimitHistory,
            isCallBackEnabled,
            if (isRTL == CustomAlignment.RTL) LanguageAlignment.RTL else LanguageAlignment.DEFAULT,
            headerTitleFont,
            headerDescriptionFont,
            sendButtonDrawable
        )*/
    }

    fun launchBot(requestCode: Int? = null) {
        /*requestCode?.let {
            ChatBotConfig.getInstance().launchBot(it)
        } ?: kotlin.run {
            ChatBotConfig.getInstance().launchBot()
        }*/
    }

    fun setDataAttributes(attrs: Any?) {
//        ChatBotConfig.getInstance().setDataAttributes(attrs)
    }

    fun clearCache(context: Context?) {
//        ChatBotConfig.getInstance().clearCache(context)
    }

    fun getBotFragment(): Fragment {
        return Fragment()
    }
}